<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 9:35 AM
 */

namespace Pc\Util;


class Url {
    public static function full($s = null, $secure = false)
    {
        if ($s === null) {
            $s = $_SERVER;
        }
        $ssl = self::isSecure($s);
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
        $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];
        $port = stristr($host, ':') === false ? $port : '';
        if ($secure) {
            $protocol = 'https';
            $port = '';
        }
        return $protocol . '://' . $host . $port . $s['REQUEST_URI'];
    }

    public static function isSecure($s = null)
    {
        if ($s === null) {
            $s = $_SERVER;
        }
        return ((isset($s['HTTPS']) && !empty($s['HTTPS']) && $s['HTTPS'] == 'on') || ($s['SERVER_PORT'] == '443')) ? true:false;
    }

    public static function removeFromQueryString($url, $key)
    {
        $url = preg_replace('/(?:&|(\?))' . $key . '[=]*[^&]*(?(1)&|)?/i', "$1", $url);
        return $url;
    }
} 