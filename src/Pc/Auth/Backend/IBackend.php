<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:33 AM
 */

namespace Pc\Auth\Backend;


interface IBackend {
    /**
     * @param $username
     * @param $password
     * @return false|\Pc\Auth\Model\IUser
     */
    public function authenticate($username, $password);

    /**
     * @param \Pc\Auth\Model\IUser $user
     * @return bool
     */
    public function login($user);

    /**
     * Reset user object
     * @return null
     */
    public function logout();

    /**
     * @param array $options
     * @return null
     */
    public function initOptions($options);

    /**
     * @return \Pc\Auth\Model\IUser
     */
    public function getUser();

    /**
     * @return bool
     */
    public function isAuthenticated();

    /**
     * @return array
     */
    public function getErrors();
}