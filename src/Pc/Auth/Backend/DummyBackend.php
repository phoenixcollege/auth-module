<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:37 AM
 */

namespace Pc\Auth\Backend;

use Pc\Auth\Model\GenericUser;

class DummyBackend extends AbstractBackend implements IBackend
{
    /**
     * @param $username
     * @param $password
     * @return false|\Pc\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        $this->user = new GenericUser(array('id' => 1, 'username' => $username));
        return $this->isAuthenticated();
    }
}