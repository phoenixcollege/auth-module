<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/7/14
 * Time: 8:34 AM
 */

namespace Pc\Auth\Backend;


class AdBackend extends AbstractBackend implements IBackend
{
    /**
     * @var \adLDAP\adLDAP
     */
    protected $ad;

    /**
     * @var array
     */
    protected $options = array();

    /**
     * @param $username
     * @param $password
     * @return false|\Pc\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        try {
            $res = $this->ad->authenticate($username, $password);
            if (!$res) {
                $this->errors[] = 'Invalid username or password.';
            }
            else {
                $this->user = $this->getById($username);
            }
        }
        catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return $this->isAuthenticated();
    }

    public function initOptions($options, $ad = null)
    {
        if ($ad === null) {
            $ad = new \adLDAP\adLDAP($options);
        }
        $this->setAdProvider($ad);
        $this->options = $options;
        parent::initOptions($options);
    }

    public function setAdProvider(\adLDAP\adLDAP $ad)
    {
        $this->ad = $ad;
    }

    public function getById($identifier)
    {
        $infoArray = null;
        $info = $this->ad->user()->infoCollection($identifier, array('*'));
        if ($info) {
            $infoArray = $this->setInfoArray($info);
            $infoArray[constant($this->userClass . '::ID_FIELD')] = $identifier;
        }
        return new $this->userClass((array) $infoArray);
    }

    protected function setInfoArray($info)
    {
        /*
        * in $options set the fields array with each value
        * as a field you want from active directory
        * If you have 'username' => 'samaccountname' it will set the $info['username'] = $infoCollection->samaccountname
        * refer to the adLDAP docs for which fields are available.
        */
        $infoA = array();
        if (!empty($this->options['fields'])) {
            foreach ($this->options['fields'] as $k => $field) {
                if ($k == 'groups') {
                    $infoA[$k] = $this->getAllGroups($info->memberof);
                }elseif ($k == 'primarygroup') {
                    $infoA[$k] = $this->getPrimaryGroup($info->distinguishedname);
                }else{
                    $infoA[$k] = $info->$field;
                }
            }
        }else{
            //if no fields array present default to username and displayName
            $infoA[constant($this->userClass . '::USERNAME_FIELD')] = $info->samaccountname;
            $infoA['displayname'] = $info->displayName;
            $infoA['primarygroup'] = $this->getPrimaryGroup($info->distinguishedname);
            $infoA['groups'] = $this->getAllGroups($info->memberof);
        }
        return $infoA;
    }

    protected function getAllGroups($groups)
    {
        $grps = '';
        if (!is_null($groups)) {
            if (!is_array($groups)) {
                $groups = explode(',', $groups);
            }
            foreach($groups as $ngroups) {
                $ngroupexp = explode(',', $ngroups);
                $groupstring = reset($ngroupexp);
                if (substr($groupstring, 0, 3) === 'CN=') {
                    $grps[] = substr($groupstring, 3);
                }
            }
        }
        return $grps;
    }

    protected function getPrimaryGroup($dn)
    {
        $groups = explode(',', $dn);
        return substr($groups[1], '3');
    }

}