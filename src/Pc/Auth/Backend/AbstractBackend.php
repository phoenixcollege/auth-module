<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 2:02 PM
 */

namespace Pc\Auth\Backend;


class AbstractBackend
{
    /**
     * @var \Pc\Auth\Model\IUser
     */
    protected $user;

    protected $errors = array();

    protected $userClass = '\Pc\Auth\Model\GenericUser';

    /**
     * @param \Pc\Auth\Model\IUser $user
     * @return bool
     */
    public function login($user)
    {
        $this->user = $user;
        return true;
    }

    /**
     * reset user object
     */
    public function logout()
    {
        $this->user = new $this->userClass();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return \Pc\Auth\Model\IUser
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = new $this->userClass();
        }
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->getUser()->getId() ? true : false;
    }

    /**
     * @param array $options
     * @return null
     */
    public function initOptions($options)
    {
        if (isset($options['userClass'])) {
            $this->userClass = $options['userClass'];
        }
    }
} 