<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:02 AM
 */

namespace Pc\Auth\Model;


interface IUser {
    public function getId();
    public function getUsername();
    public function __get($name);
    public function __set($key, $value);
    public function toArray();
} 