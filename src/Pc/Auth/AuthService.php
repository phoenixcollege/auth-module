<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 2:04 PM
 */

namespace Pc\Auth;


use Smorken\Service\Service;

class AuthService extends Service {

    public function start()
    {
        $this->name = 'auth';
    }

    public function load()
    {
        $app = $this->app;
        $this->app->instance($this->getName(), function($c) {
            return new \Pc\Auth\AuthHandler($c['config'], $c['session'], \Pc\Util\Url::full($_SERVER));
        });
    }
} 