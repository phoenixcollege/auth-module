<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 1:02 PM
 */

namespace Pc\Auth;

use Pc\Util\Url;
use Smorken\Config\Config;

class AuthHandler
{

    /**
     * @var bool whether to use the auth handler or not
     */
    protected $useHandler;
    /**
     * @var string
     */
    protected $returnUrl;
    /**
     * @var \Smorken\Config\Config
     */
    protected $config;
    /**
     * @var \Pc\Auth\Backend\IBackend
     */
    protected $backend;
    /**
     * @var \Smorken\Session\SessionHandler
     */
    protected $session;

    /**
     * @var \Pc\Util\Redirect
     */
    protected $redirector;

    /**
     * @param \Smorken\Config\Config $config
     */
    public function __construct(\Smorken\Config\Config $config, \Smorken\Session\SessionHandler $session, $returnUrl)
    {
        $this->useHandler = $config['auth.authenticate'] !== null ? $config['auth.authenticate'] : true;
        $this->setSession($session);
        $this->setConfig($config);
        $this->setReturnUrl($returnUrl);
        $this->init();
        $this->checkLogout();
        $this->checkSsl();
    }

    public function checkLogout()
    {
        if (isset($_GET[$this->config->get('auth.logout_key', 'logout')]) && $this->isAuthenticated()) {
            $this->logout();
        }
    }

    public function checkSsl()
    {
        if ($this->useHandler && $this->config['auth.require_ssl']) {
            if (isset($_SERVER) && isset($_SERVER['SERVER_PROTOCOL'])) {
                if (!Url::isSecure($_SERVER)) {
                    $this->getRedirector()->go(Url::full($_SERVER, true));
                }
            }
        }
    }

    protected function init()
    {
        $this->createBackendFromConfig();
        $this->initSessionFromConfig();
        $this->createRedirector();
    }

    protected function createRedirector()
    {
        $this->setRedirector(new \Pc\Util\Redirect());
    }

    protected function createBackendFromConfig()
    {
        $bc = $this->config->get('auth.backend', array());
        $backendClass = isset($bc['class']) ? $bc['class'] : '\Pc\Auth\Backend\DummyBackend';
        $options = isset($bc['options']) ? $bc['options'] : array();
        $backend = new $backendClass();
        $backend->initOptions($options);
        $this->setBackend($backend);
        if (!$this->getBackend()) {
            throw new AuthHandlerException("Backend must be instantiated.");
        }
    }

    protected function initSessionFromConfig()
    {
        $sc = $this->config->get('auth.session', array());
        $namespace = isset($sc['namespace']) ? $sc['namespace'] : 'auth';
        $this->getSession()->getAdapter()->setNamespace($namespace);
    }

    protected function loadUserFromSession()
    {
        $userserial = $this->getSession()->user;
        if ($userserial) {
            $user = unserialize($userserial);
            return $this->getBackend()->login($user);
        }
        return false;
    }

    /**
     * @param $user \Pc\Auth\Model\IUser
     */
    protected function saveUserToSession($user)
    {
        $serialuser = serialize($user);
        $this->getSession()->user = $serialuser;
    }

    public function isAuthenticated()
    {
        if ($this->getBackend()->isAuthenticated() && $this->useHandler) {
            return true;
        }
        $this->loadUserFromSession();
        $is_auth = $this->getBackend()->isAuthenticated();
        if ($this->useHandler) {
            return $is_auth;
        } else {
            return true;
        }
    }

    public function user()
    {
        return $this->getBackend()->getUser();
    }

    /**
     * @param \Smorken\Config\Config $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return \Smorken\Config\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    public function authenticate($username, $password)
    {
        $this->getSession()->clear();
        $this->getSession()->regenerate();
        $result = $this->getBackend()->authenticate($username, $password);
        if ($result === true) {
            $this->saveUserToSession($this->user());
            if ($this->config->get('auth.redirect', false) === true && $this->getReturnUrl()) {
                $this->getRedirector()->go($this->getReturnUrl(), $this->config->get('auth.logout_key', 'logout'));
            }
        }
        return $result;
    }

    public function logout()
    {
        $this->getSession()->destroy();
        $this->getBackend()->logout();
    }

    public function hasErrors()
    {
        return count($this->getErrors()) > 0;
    }

    public function getErrors()
    {
        return $this->getBackend()->getErrors();
    }

    /**
     * @param \Pc\Auth\Backend\IBackend $backend
     */
    public function setBackend($backend)
    {
        $this->backend = $backend;
    }

    /**
     * @return \Pc\Auth\Backend\IBackend
     */
    public function getBackend()
    {
        return $this->backend;
    }

    /**
     * @param \Smorken\Session\SessionHandler $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * @return \Smorken\Session\SessionHandler
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * @return \Pc\Util\Redirect
     */
    public function getRedirector()
    {
        return $this->redirector;
    }

    /**
     * @param \Pc\Util\Redirect $redirector
     */
    public function setRedirector($redirector)
    {
        $this->redirector = $redirector;
    }
} 