<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 2:26 PM
 */
if (!$app['auth']->isAuthenticated()) {
    $authenticated = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $post = filter_input_array(INPUT_POST, array(
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
            'redirectTo' => FILTER_SANITIZE_URL,
        ));
        $app['auth']->setReturnUrl($post['redirectTo']);
        $authenticated = $app['auth']->authenticate($post['username'], $post['password']);
    }
    if (!$authenticated) {
        require_once 'html.php';
        exit;
    }
}
