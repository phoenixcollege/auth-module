<?php
/**
 * App specific configuration items
 * Copy this file to your application's base/config
 * and modify for your use.
 */
return array(
    'debug' => false,
    'services' => array(
        'Pc\Auth\AuthService',
    ),
);