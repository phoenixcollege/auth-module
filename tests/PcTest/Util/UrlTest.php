<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 9:44 AM
 */

use \Pc\Util\Url;

class UrlTest extends PHPUnit_Framework_TestCase {

    public function testUrlGenerator()
    {
        $url = Url::full($this->setupServer());
        $this->assertEquals('http://test.server/index.php', $url);
    }

    public function testUrlGeneratorSsl()
    {
        $url = Url::full($this->setupServerSsl());
        $this->assertEquals('https://test.server/index.php', $url);
    }

    public function testUrlGeneratorWithPort()
    {
        $url = Url::full($this->setupServerPort());
        $this->assertEquals('http://test.server:8000/index.php', $url);
    }

    public function testRemoveFromQueryStringHasValue()
    {
        $url = 'http://test.local/index.php?test1=foo&test2=bar';
        $expected = 'http://test.local/index.php?test2=bar';
        $this->assertEquals($expected, Url::removeFromQueryString($url, 'test1'));
    }

    public function testRemoveFromQueryStringNoValue()
    {
        $url = 'http://test.local/index.php?test1&test2=bar';
        $expected = 'http://test.local/index.php?test2=bar';
        $this->assertEquals($expected, Url::removeFromQueryString($url, 'test1'));
    }

    public function testForceSsl()
    {
        $url = Url::full($this->setupServerPort(), true);
        $this->assertEquals('https://test.server/index.php', $url);
    }

    protected function setupServer()
    {
        $s = array(
            'HTTPS' => '',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'SERVER_PORT' => '80',
            'SERVER_NAME' => 'test.server',
            'REQUEST_URI' => '/index.php'
        );
        return $s;
    }

    protected function setupServerSsl()
    {
        $s = $this->setupServer();
        $s['HTTPS'] = 'on';
        $s['SERVER_PORT'] = '443';
        return $s;
    }

    protected function setupServerPort()
    {
        $s = $this->setupServer();
        $s['SERVER_PORT'] = '8000';
        return $s;
    }
}
 