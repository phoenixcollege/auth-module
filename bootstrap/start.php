<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:02 AM
 */
$container = new Pimple\Container();
$app = Smorken\Application\App::getInstance($container);

$app->addPaths(require __DIR__ . '/paths.php');
$app->start();

define('DEBUG', $app['config']['app.debug'], false);

return $app;